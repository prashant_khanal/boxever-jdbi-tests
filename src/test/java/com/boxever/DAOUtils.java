package com.boxever;


import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface DAOUtils {

    @SqlQuery("SELECT count(*) FROM users")
    int countUsers();

    @SqlUpdate("DELETE users")
    int deleteUsers();
}
