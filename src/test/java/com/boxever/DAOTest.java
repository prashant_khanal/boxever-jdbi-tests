package com.boxever;

import javax.sql.DataSource;
import org.flywaydb.core.Flyway;
import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.Server;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.skife.jdbi.v2.DBI;

public abstract class DAOTest {
    public static final String JDBC_URL = "jdbc:h2:mem:test_mem;MODE=PostgreSQL;DB_CLOSE_DELAY=-1";

    private static Server server;
    protected static DBI jdbi;

    @BeforeClass
    public static void initDB() throws Exception {
        server = Server.createTcpServer();

        initSchema();
        DataSource dataSource = JdbcConnectionPool.create(JDBC_URL, "sa", "");
        jdbi = new DBI(dataSource);
    }

    private static void initSchema() {
        Flyway flyway = new Flyway();
        flyway.setDataSource(JDBC_URL, "sa", "");
        flyway.migrate();
    }

    @AfterClass
    public static void closeDB() throws Exception {
        server.shutdown();
    }
}
