package com.boxever;

import static org.junit.Assert.assertThat;

import java.util.UUID;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

public class UserDAOTest extends DAOTest {

    private UserDAO userDAO;
    private DAOUtils daoUtils;

    @Before
    public void setUp() throws Exception {
        userDAO = jdbi.onDemand(UserDAO.class);
        daoUtils = jdbi.onDemand(DAOUtils.class);

        daoUtils.deleteUsers();
    }

    @Test
    public void shouldInsertAUser() throws Exception {
        userDAO.insert(createUser("H2 is so fast!","h2@fast.com"));

        assertThat(daoUtils.countUsers(), Is.is(1));
    }

    @Test
    public void shouldInsertTwoUser() throws Exception {
        userDAO.insert(createUser("Flyway Rocks", "flyway@rocks.com"));
        userDAO.insert(createUser("JDBI is so Clean", "clean@jdbi.com"));

        assertThat(daoUtils.countUsers(), Is.is(2));
    }

    private User createUser(String name, String email) {
        User user = new User();
        final UUID ref = UUID.randomUUID();
        user.setRef(ref);
        user.setName(name);
        user.setEmail(email);
        return user;
    }
}